package positive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PragmaticShopPositiveRegistry {
    public static WebDriver driver;

    @BeforeMethod
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", "E:\\Web_Drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shop.pragmatic.bg/");
    }
    @Test
    public static void successfulReg(){
        driver.findElement(By.cssSelector("li.dropdown")).click();
        driver.findElement(By.linkText("Register")).click();
        Assert.assertTrue(driver.getTitle().contains("Register Account"));
        driver.findElement(By.id("input-firstname")).sendKeys("Random");
        driver.findElement(By.id("input-lastname")).sendKeys("Testnames");
        driver.findElement(By.id("input-email")).sendKeys("randomemail@abv.com");
        driver.findElement(By.id("input-telephone")).sendKeys("0899238913");
        driver.findElement(By.id("input-password")).sendKeys("parola123");
        driver.findElement(By.id("input-confirm")).sendKeys("parola123");
        WebElement radiobuttonyes = driver.findElement(By.cssSelector("label.radio-inline input[value='1']"));
        WebElement radiobuttonno = driver.findElement(By.cssSelector("label.radio-inline input[value='0']"));
        radiobuttonyes.click();
        Assert.assertTrue(radiobuttonyes.isSelected());
        Assert.assertFalse(radiobuttonno.isSelected());
        WebElement checkboxprivacypolicy = driver.findElement(By.cssSelector("input[type='checkbox']"));
        checkboxprivacypolicy.click();
        Assert.assertTrue(checkboxprivacypolicy.isSelected());
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Assert.assertTrue(driver.getTitle().contains("Your Account Has Been Created!"));
        driver.findElement(By.cssSelector("div.buttons div.pull-right a")).click();
    }

    @AfterMethod
    public static void quitBrowser() {
       driver.quit();
    }
}
