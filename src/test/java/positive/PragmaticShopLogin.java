package positive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

public class PragmaticShopLogin {
    public static WebDriver driver;

    @BeforeMethod
    public static void setUp(){
        System.setProperty("webdriver.chrome.driver", "E:\\Web_Drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shop.pragmatic.bg/");
    }

    @Test
    public static void logIn(){
        driver.findElement(By.cssSelector("li.dropdown")).click();
        driver.findElement(By.linkText("Login")).click();
        Assert.assertTrue(driver.getTitle().contains("Account Login"));
        driver.findElement(By.id("input-email")).sendKeys("randomemail@abv.com");
        driver.findElement(By.id("input-password")).sendKeys("parola123");
        driver.findElement(By.cssSelector("form input[type='submit']")).click();
        Assert.assertTrue(driver.getTitle().contains("My Account"));
    }

    @AfterMethod
    public static void quitBrowser() {
        driver.quit();
    }
}
