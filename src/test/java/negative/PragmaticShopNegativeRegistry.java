package negative;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class PragmaticShopNegativeRegistry {
    public static WebDriver driver;

    @BeforeMethod
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", "E:\\Web_Drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shop.pragmatic.bg/");
    }

    @Test
    public static void failedRegistry(){
        driver.findElement(By.cssSelector("li.dropdown")).click();
        driver.findElement(By.linkText("Register")).click();
        Assert.assertTrue(driver.getTitle().contains("Register Account"));
        driver.findElement(By.cssSelector("input[type='submit']")).click();
        Assert.assertTrue(driver.findElement(By.cssSelector("div[class='alert alert-danger alert-dismissible']")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.cssSelector("input#input-firstname + div.text-danger")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.cssSelector("input#input-lastname + div.text-danger")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.cssSelector("input#input-email + div.text-danger")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.cssSelector("input#input-telephone + div.text-danger")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.cssSelector("input#input-password + div.text-danger")).isDisplayed());
    }

    @AfterMethod
    public static void quitBrowser(){
        driver.quit();
    }
}
